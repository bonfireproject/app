# Bonfire

The free, open-source, modern and secure alternative to Discord.

**Please note a working beta will take a very, very long time. Be patient - if you want to contribute get in touch via [Revolt](https://app.revolt.chat/invite/Z7qhw2n0) or [Discord](https://discord.gg/REUZRghEJv).**

## Installation

Requirements: an account on supabase.io, Python, pip, and a server (see Wiki/Supported Computers).
Optional Extras: reCAPTCHA or hCaptcha account (recommended!!!)

Fill .env with your secrets.
Other directions pending.

## Contributing
Merge requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[Bonfire, and all separate components are licensed under the MIT license.](https://choosealicense.com/licenses/mit/).
